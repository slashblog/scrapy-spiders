# (C) Copyright 2021

from fuzzywuzzy import fuzz
from fuzzywuzzy import process

import argparse
import ast
import sys

def find_duplicates(lines):
    titles = []
    for line in lines:
        line = line.replace('Miniature Sheet', '')
        titles.append(line)
    for i, title in enumerate(titles):
        print(title)
        max_score = 0
        predicted = ''
        for j in range(i+1, len(titles)):
            score = fuzz.token_sort_ratio(title, titles[j])
            if score > max_score:
                max_score = score
                predicted = titles[j]
            if score > 80:
                print(title + ' <> ' + titles[j] + ' :: ' + str(score))
        print(title + ' => ' + predicted + ' :: ' + str(max_score))


if __name__ == '__main__':
    if len(sys.argv) == 0:
        print('Path not provided')
        exit(1)

    path = sys.argv[1]
    input = path + '/titles.txt'
    output = path + '/titles_duplicates.json'

    try:
        print('Input: ' + input)
        with open(input,'r') as file:
            titles = ast.literal_eval(file.read())
            find_duplicates(titles)
    except:
        print(input + ' files not found')
        exit(1)
