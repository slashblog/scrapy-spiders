# (C) Copyright 2020

# scrapy crawl silvera

import json

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem
from scrapy.http import Request
from scrapy.selector import Selector

class SilveraSpider(BaseSpider):
    name = 'silvera'
    categories = [{
        'metal': 'silver',
        'type': 'coin',
        'id': 8
    }, {
        'metal': 'silver',
        'type': 'bar',
        'id': 7
    }, {
        'metal': 'gold',
        'type': 'coin',
        'id': 10
    }, {
        'metal': 'gold',
        'type': 'bar',
        'id': 9
    }]

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name

    def get_urls(self):
        pass
    
    def start_requests(self):
        for category in self.categories:
            yield self.start_requests_for_category_page(category['id'], 1)

    def start_requests_for_category_page(self, category, page):
        url = 'https://silvera.co.in/app/api/search'
        form_data = {
            "sub_category":[category],
            "weight": {
                "min":1,
                "max":1000
            },
            "occasion": [],
            "brand": [2],
            "price_range": {
                "min":1,
                "max":10000000
            },
            "sort_by":"",
            "divine_collection":0,
            "page":page
        }
        return Request(url, method='POST', 
                body=json.dumps(form_data), 
                headers={'Content-Type':'application/json'}, meta={'category':category} )        

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        data = json.loads(response.body)
        if data['error'] == True:
            print('Error: ' + data['errorMessage'])
            return

        data = data['data']
        page = data['page']
        total_page = data['total_page']
        print("Page: " + str(page) + " out of: " + str(total_page))

        for product in data['products']:
            index = str(product['id'])
            product['index'] = index
            product['manufacturer'] = 'MMTC-PAMP'
            product['url'] = 'https://www.silvera.co.in/product/' + product['slug']
            product['title'] = product['product_name']
            product[self.key_available] = True

            for item in Selector(text=product['product_specification']).xpath('//p/text()').getall():
                splits = item.split('-')
                if len(splits) >= 2:
                    product[splits[0].strip()] = splits[1].strip()

            self.price_data.append({
                'id': index,
                'price': product['price']
            })

            product[self.key_images] = []
            image_urls = product['image']
            for image_url in image_urls:
                image = {}
                image['original_url'] = image_url
                image['url'] = self.get_image_url(image_url)
                product[self.key_images].append(image)
                    
            yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

            self.items[self.get_0_index(index)] = product

        if page < total_page:
            print("Proessing page " + str(page + 1))
            yield self.start_requests_for_category_page(response.meta['category'], page + 1)

    def save_price_path(self):
        return self.save_price_base_path() + '/silvera-data.json'

    def save_price(self):
        self.save_price_daily()
