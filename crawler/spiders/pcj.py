# (C) Copyright 2020 

# scrapy crawl pcj

import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class PcjSpider(BaseSpider):
    name = "pcj"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.pcjeweller.com/icc-official-world-cup-2019-gold-medal-collection-set.html',
            'https://www.pcjeweller.com/icc-official-world-cup-2019-team-blue-5g-999-gold-medal.html',
            'https://www.pcjeweller.com/icc-official-world-cup-2019-team-blue-10g-999-gold-medal.html',
            'https://www.pcjeweller.com/icc-official-world-cup-2019-the-striker-10g-999-gold-medal.html',
            'https://www.pcjeweller.com/icc-official-world-cup-2019-team-blue-31-1g-999-silver-medal.html',
            'https://www.pcjeweller.com/icc-official-world-cup-2019-the-striker-31-1g-999-silver-medal.html',
            'https://www.pcjeweller.com/icc-official-world-cup-2019-team-blue-15-5g-999-silver-medal.html',
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        index = response.xpath("//input[@id='selectedProductOption']/@value").get()

        item = {}
        item['index'] = index
        item['title'] = response.xpath("//div[contains(@class, 'productDetails')]/div/div/div/h1/text()").get()
        item['sku'] = response.xpath("//div[contains(@class, 'productDetails')]/div/div/div/input[@id='product_sku']/@value").get()
        item['description'] = ''.join(response.xpath("//div[contains(@class, 'productDetails')]/div/div/div[contains(@class, 'pdt-text-color')]/span/text()").getall())
        item['url'] = response.request.url
        item[self.key_available] = True

        price = self.extract_number(response.xpath("//div[@class='price-outer']/span[@id='product-price']/text()").get().strip())
        item['price'] = price
        self.price_data.append({
            'id': index,
            'price': price
        })

        keys = list(map(lambda x: x.strip(), response.xpath("//li/div[contains(@class, 'spec-label')]/text()").getall()))
        values = list(map(lambda x: x.strip(), response.xpath("//li/div[contains(@class, 'spec-detail')]/text()").getall()))

        for i, key in enumerate(keys):
            if i < len(values):
                item[key] = values[i]

        item[self.key_images] = []
        image_urls = response.xpath("//div[contains(@class, 'thumb-view')]/div/div[@class='thumb']/a/@data-zoom-image").getall()
        for image_url in image_urls:
            image = {}
            image['original_url'] = image_url
            image['url'] = self.get_image_url(image_url)
            item[self.key_images].append(image)

        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

        self.items[self.get_0_index(index)] = item

    def save_price_path(self):
        return self.save_price_base_path() + '/pcj-data.json'

    def save_price(self):
        self.save_price_daily()
