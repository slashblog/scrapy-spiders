# (C) Copyright 2020

# scrapy crawl worldwidemint

import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class WorldWideMint(BaseSpider):
    name = "worldwidemint"

    def __init__(self, destination):
        super().__init__(destination, destination, True)
        self.old_items = self._get_existing_items()
    
    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.worldwide-mint.com/collections/miniature-sheet'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for url in response.xpath("//ul[@class='pagination-custom']/li/a/@href").getall():
            url = 'https://www.worldwide-mint.com' + url
            yield scrapy.Request(url=url, callback=self.parse)

        for url in response.xpath("//a[@class='product-grid-item']/@href").getall():
            url = response.urljoin(url)
            print('Fetching item details: ' + url)
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        print('Inside parse for: ' + response.request.url)

        index = response.xpath("//select[@name='id']/option/@value").get()
        title = response.xpath("//h1[@class='h2']/text()").get()
        price = response.xpath("//small[@aria-hidden='true']/text()").get()
        image_url = 'https:' + response.xpath("//div/img[@data-zoom]/@data-zoom").get()
        #print('Image url: ' + image_url)

        item = {}
        item['item'] = index
        item['title'] = title    
        item['price'] = price
        item['description'] = title
        item['original_url'] = image_url
        item['actual_url'] = response.request.url
        item['image_url'] = self.get_image_url(image_url)
        item[self.key_available] = True

        self.items[str(item['item'])] = item
        self.add_title(title)

        image_urls = []
        image_urls.append(image_url)
        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        pass

    def save_price(self):
        pass
