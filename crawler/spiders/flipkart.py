# (C) Copyright 2020 

# scrapy crawl flipkart

import re
import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class FlipkartSpider(BaseSpider):
    name = "flipkart"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.flipkart.com/gold-silver-coins/mmtc-pamp-india-pvt-ltd~brand/pr?sid=mcr,73x,ydh&marketplace=FLIPKART&otracker=product_breadCrumbs_MMTC-PAMP+India+Pvt+Ltd+Coins+%26+Bars',
            'https://www.flipkart.com/mmtc-pamp-india-pvt-ltd-om-lakshmi-24-9999-yellow-gold-pendant/p/itma9c3249902082',
            #'https://www.flipkart.com/gold-silver-coins/kundan~brand/pr?sid=mcr,73x,ydh&marketplace=FLIPKART&otracker=product_breadCrumbs_Kundan+Coins+%26+Bars',
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        if 'marketplace' in response.request.url:
            print('Market place detected')

            urls = response.xpath("//script[@id='is_script']").re(r'"baseUrl":"([^"]+)"')
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse)
        else:            
            item = {}
            index = re.findall(r'/([^/]+)$',response.request.url)[0]
            index = re.sub(r'[?].+$', '', index)
            item['index'] = index
            item['title'] = response.xpath("//div/h1/span/text()").get().strip()
            if len(response.xpath("//div/div/div/text()").re(r'^₹(.*)')) > 0:
                item[self.key_available] = True
                item['price'] = response.xpath("//div/div/div/text()").re(r'^₹(.*)')[0].replace(',','')
            else:
                item[self.key_available] = False
            vendors = response.xpath("//div[@id='sellerName']/span/span/text()").getall()
            if len(vendors) > 0:
                item['vendor_name'] = vendors[0].strip()
            item['url'] = response.request.url

            for specs_row in response.xpath("//div[@class='row']"):
                divs = specs_row.xpath("./div/text()").getall()
                if len(divs) < 2:
                    continue

                key = divs[0].strip()
                value = divs[1].strip()

                if len(key) == 0 or len(value) == 0:
                    continue

                item[key] = value

            if 'price' in item:
                self.price_data.append({
                    'asin': index,
                    'name': item['title'],
                    'price': item['price']
                })

            item[self.key_images] = []
            original_image_urls = response.xpath("//script[@id='is_script']").re(r'"url":"([^"]+.jpeg[^"]+)"')
            image_urls = []
            for original_image_url in original_image_urls:
                image_url = original_image_url.replace('{@width}', '880').replace('{@height}', '1056').replace('{@quality}', '50')
                image_urls.append(image_url)
                item[self.key_images].append({ 'original_url': image_url, 'url': self.get_image_url(image_url) })

            self.items[index] = item

            yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        return self.save_price_base_path() + '/flipkart-data.json'

    def save_price(self):
        self.save_price_hourly_hyphenated()
