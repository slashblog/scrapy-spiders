# (C) Copyright 2020 

# scrapy crawl dollar_inr

import json
import os
import re
import scrapy

from datetime import datetime
from scrapy import signals
from scrapy.utils.python import to_bytes

class DollarInrSpider(scrapy.Spider):
    name = "dollar_inr"
    items = {}
    file_name = "data.json"

    def __init__(self, destination):
        self.destination = destination

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(DollarInrSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
        return spider

    def spider_closed(self, spider):
        if not self.items:
            print('No data to save. Skipping')
        else:
            json_string = json.dumps(self.items, indent=4, sort_keys=False)
            print("Writing data to file ...")
            with open(os.path.join(self.destination, self.file_name), "wb") as file:
                file.write(to_bytes(json_string))

    def start_requests(self):
        for i in range(2010, 2021):
            url = 'https://www.exchangerates.org.uk/USD-INR-spot-exchange-rates-history-' + str(i) + '.html'
            print("Processing " + url)
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        print("Inside parse for: " + response.request.url)

        date = ''

        for i, row in enumerate(response.xpath("//table[@id='hist']/tr[@class='colone']/td")):
            if (i + 1) % 3 == 1:
                date = datetime.strptime(row.xpath(".//text()").get(), '%A %d %B %Y').strftime('%Y-%m-%d')
            elif (i + 1) % 3 == 2:
                self.items[date] = float(re.sub('[^.0-9]', '', row.xpath(".//text()").get().split('=')[1]))

