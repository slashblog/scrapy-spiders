# (C) Copyright 2020

# scrapy crawl spmcil

import re
import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class KundanSpider(BaseSpider):
    name = "spmcil"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            "https://spmcil.com/Interface/all-products.aspx",
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for city_row in response.xpath("//div[@class='row products']"):
            city_name = city_row.xpath("./div/a/div/div/div/img/@src").get().replace(".jpg", "").replace("images/", "").capitalize()

            for city_item in city_row.xpath(".//div/div/div/div[@class='item active' or @class = 'item']/div"):
                item_name = city_item.xpath(".//div/p[@class='ele']/text()").get()
                print("Identified item: " + item_name)

                details_link_src = city_item.xpath(".//div/p/a[@class='btn']/@href").get()
                details_link = response.urljoin(details_link_src)
                is_counter = details_link_src.find("CounterProduct") >= 0
                index = details_link.split("=")[1]

                item = {
                    "index": index,
                    "name": item_name.title().replace(" A ", " a ").replace(" An ", " an ").replace(" The ", " the ").replace(" And ", " and ").replace(" Of ", " of "),
                    "name_src": item_name,
                    "city": city_name,
                    "counter_item": is_counter,
                    "listed": True,
                    "details_link_src": details_link_src,
                    "details_link": details_link,
                }

                yield scrapy.Request(url=details_link, callback=self.parse_item, meta={'item' : item})

    def parse_item(self, response):
        print('Inside parse for: ' + response.request.url)

        item = response.meta.get('item')

        item['url'] = response.request.url

        index = response.xpath("//input[@id='ctl00_ContentPlaceHolder1_rptGridProduct_ctl00_SellableProductID']/@value").get()

        item_prices = {
            self.key_index: self.get_0_index(index)
        }
        item_prices['price_unc'] = item['price_unc'] = response.xpath("//input[@id='ctl00_ContentPlaceHolder1_rptGridProduct_ctl00_UncircularRatewithTax']/@value").get()
        item_prices['price_proof'] = item['price_proof'] = response.xpath("//input[@id='ctl00_ContentPlaceHolder1_rptGridProduct_ctl00_ProofRatewithTax']/@value").get()

        count = response.xpath("//input[@id='ctl00_ContentPlaceHolder1_rptGridProduct_ctl00_SellableProductCount']/@value").get()
        if count is None:
            item['count_unc'] = -1
            item_prices['count_unc'] = -1
        else:
            item['count_unc'] = count
            item_prices['count_unc'] = count

        count = response.xpath("//input[@id='ctl00_ContentPlaceHolder1_rptGridProduct_ctl00_ProofQuantity']/@value").get()
        if count is None:
            item['count_proof'] = -1
            item_prices['count_proof'] = -1
        else:
            item['count_proof'] = count
            item_prices['count_proof'] = count
        
        if item['count_proof'] != 0 or item['count_unc'] != 0:
            item[self.key_available] = True
        else:
            item[self.key_available] = False

        self.price_data.append(item_prices)
        
        item['status'] = response.xpath("//div[@class='product-title']/b/text()").get()

        image_urls = []
        item[self.key_images] = []
        for img_src_path in response.xpath("//div[contains(@class, 'container service1-items')]/center/a/img/@src").getall():
            img_url = response.urljoin(img_src_path.replace("\\", "/").replace(" ", "%20"))
            image = {
                'original_url': img_src_path,
                'converted_url': img_url,
                'url': self.get_image_url(img_url)
            }
            item[self.key_images].append(image)
            image_urls.append(img_url)

        details = {}

        for i, info in enumerate(response.xpath("//section[@class='container product-info']")):
            if i == 0:
                details["theme"] = " ".join(info.xpath(".//p/descendant-or-self::text()").getall()).strip()
            else:
                table_selector = info.xpath(".//table[@id='tablePreview']")
                if len(table_selector) > 0:
                    details["coins"] = {}
                    details["coins"]["columns"] = table_selector.xpath(".//thead/tr/th/text()").getall()
                    details["coins"]["rows"] = []

                    for row_selector in table_selector.xpath(".//tbody/tr"):
                        row = []
                        for column_selector in row_selector.xpath(".//td"):
                            column = ', '.join(map(lambda x: x.strip(), column_selector.xpath(".//descendant-or-self::text()").getall()))
                            if re.match("\\(\\d\\)", column):
                                continue
                            else:
                                row.append(column)
                        if len(row) > 0:
                            details["coins"]["rows"].append(row)
                details["product_specification"] = " ".join(info.xpath(".//p/descendant-or-self::text()").getall()).strip()

        item["details"] = details

        self.items[self.get_0_index(index)] = item

        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        return self.save_price_base_path() + '/spmcil-data.json'

    def save_price(self):
        self.save_price_on_change()
