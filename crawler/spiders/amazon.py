# (C) Copyright 2020

# scrapy crawl amazon

import re
import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class AmazonSpider(BaseSpider):
    name = "amazon"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.amazon.in/s?me=AZ9EEYCKMPQG6&marketplaceID=A21TJRUUN4KGV',
            #'https://www.amazon.in/s?me=A1U9BF39COA947&marketplaceID=A21TJRUUN4KGV',
            'https://www.amazon.in/s?me=A1Q2DRNRI6ILT0&marketplaceID=A21TJRUUN4KGV',
            'https://www.amazon.in/gp/product/B01M08PASC',
            'https://www.amazon.in/dp/B01HVB3PPC',
            #'https://www.amazon.in/Kundan-24KT-Yellow-Gold-Bar/dp/B077PXCFZG',
            #'https://www.amazon.in/Kundan-24KT-Yellow-Gold-Bar/dp/B077PXCFZG',
            #'https://www.amazon.in/Kundan-11-66-24KT-Yellow-Precious/dp/B07B478XFW',
            #'https://www.amazon.in/Kundan-50g-999-9-Silver-Colour/dp/B07NHHHBLD',
            #'https://www.amazon.in/Kundan-999-Silver-Diya-Coin/dp/B07PQ88322'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        if len(response.xpath("//table[@id='technicalSpecifications_section_1']/tr")) == 0:
            print('Market place detected')
            for url in response.xpath("//h2/a/@href").getall():
                url = url.replace(r'[?].*$', '')
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse)

            urls = response.xpath("//span[@cel_widget_id='MAIN-PAGINATION']/div/div/ul/li[@class='a-last']/a/@href").getall()
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse)
        else:
            item = {}

            index = response.xpath("//script[@type='text/javascript']/text()").re(r"'winningAsin': '([^']+)'")[0]
            item['index'] = index
            item['title'] = response.xpath("//div[@id='titleSection']/h1[@id='title']/span[@id='productTitle']/text()").get().strip()
            vendor = response.xpath("//div[@id='merchant-info']/a[@id='sellerProfileTriggerId']/text()").get()
            if vendor is not None:
                item['vendor_name'] = vendor.strip()
            item['url'] = response.request.url
            item['manufacturer'] = response.xpath("//div/a[@id='bylineInfo']/text()").get().strip()
            if len(response.xpath("//span[@id='priceblock_ourprice']/text()")) > 0:
                item[self.key_available] = True
                item['price'] = self.extract_number(response.xpath("//span[@id='priceblock_ourprice']/text()").get())
            else:
                item[self.key_available] = False

            for specs_row in response.xpath("//table[@id='technicalSpecifications_section_1']/tr"):
                key = ''
                if len(specs_row.xpath("./th/span/a/span/span/text()")) > 0:
                    key = specs_row.xpath("./th/span/a/span/span/text()").get().strip()
                else:
                    key = specs_row.xpath("./th/text()").get().strip()

                value = specs_row.xpath("./td/text()").get().strip()

                item[key] = value

            if 'price' in item:
                self.price_data.append({
                    'asin': index,
                    'name': item['title'],
                    'price': item['price']
                })

            item[self.key_images] = []

            image_urls = response.xpath("//script[@type='text/javascript']/text()").re(r'"hiRes":"([^"]+)"')
            for image_url in image_urls:
                item[self.key_images].append({ 'original_url': image_url, 'url': self.get_image_url(image_url) })

            self.items[index] = item

            yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        return self.save_price_base_path() + '/amazon-gold-data.json'

    def save_price(self):
        self.save_price_hourly_hyphenated()
