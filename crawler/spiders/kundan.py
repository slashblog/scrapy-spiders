# (C) Copyright 2020 

# scrapy crawl kundan

import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class KundanSpider(BaseSpider):
    name = "kundan"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.kundanrefinery.com/products/category/gold-coins',
            'https://www.kundanrefinery.com/products/category/silver-coins'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for url in response.xpath("//ul[contains(@class, 'products-grid')]/li[contains(@class, 'item')]/div/div/div/a/@href").getall():
            yield scrapy.Request(url=url, callback=self.parse_item)

        for url in response.xpath("//div[@class='pages']/ol/li/a/@href").getall():
            yield scrapy.Request(url = url, callback=self.parse)

    def parse_item(self, response):
        index = response.xpath("//form/div/input[@name='product']/@value").get()
        item = {}
        item['index'] = index
        item['url'] = response.request.url
        item['title'] = response.xpath("//div[contains(@class, 'product-shop')]/div[@class='product-shop-content']/div[@class='product-name']/h1/text()").get()
        if len(response.xpath("//div[@class='short-description']/div/text()")):
            item['description'] = response.xpath("//div[@class='short-description']/div/text()").get()
        if len(response.xpath("//div[@class='short-description']/p/text()")):
            item['description'] = response.xpath("//div[@class='short-description']/p/text()").get()
        item['sku'] = response.xpath("//div[contains(@class, 'product-shop')]/div[@class='product-shop-content']/div[@class='product-name']/span/text()").get()
        item['old-price'] = self.extract_price(response, 'old-price')
        item['special-price'] = self.extract_price(response, 'special-price')
        if len(response.xpath("//div[@class='product-type-data']/p[@class='availability out-of-stock']")) == 0:
            item[self.key_available] = True
        else:
            item[self.key_available] = False

        for row in response.xpath("//table[@class='data-table']/tbody/tr"):
            th = row.xpath("./th[@class='label']")
            if len(th) > 0:
                key = th.xpath("./text()").get().strip()
                value = row.xpath("./td[@class='data']/text()").get().strip()
                item[key] = value

        price = item['special-price'] if float(item['special-price']) > 0 else item['old-price']
        self.price_data.append({
            'id': index,
            'price': price
        })

        item[self.key_images] = []
        image_urls = response.xpath("//div[@class='product-image-gallery']/img/@src").getall()
        for image_url in image_urls:
            image = {}
            image['original_url'] = image_url
            image['url'] = self.get_image_url(image_url)
            item[self.key_images].append(image)
                
        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

        self.items[self.get_0_index(index)] = item

    def extract_price(self, response, index):
        node = response.xpath("//div[@class='product-type-data']/div[@class='price-box']/p[@class='" + index + "']/span[@class='price']/text()")
        if len(node) > 0:
            return self.extract_number(node.get())
        return '0'

    def save_price_path(self):
        return self.save_price_base_path() + '/kundan-data.json'

    def save_price(self):
        self.save_price_daily()
