# (C) Copyright 2020

# scrapy crawl coinbazaar

import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class CoinBazaarSpider(BaseSpider):
    name = "coinbazaar"
    price_items = []

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.coinbazaar.in/47-buy-mmtc-pamp-gold-coin-ingot-bar-online-price-rate-india',
            'https://www.coinbazaar.in/48-buy-mmtc-pamp-silver-coin-ingot-bar-online-price-rate-india',
            #'https://www.coinbazaar.in/525-kundan',
            #'https://www.coinbazaar.in/488-perth-mint',
            #'https://www.coinbazaar.in/507-allcollect',
            #'https://www.coinbazaar.in/583-royal-canadian-mint',
            #'https://www.coinbazaar.in/193-buy-sachin-tendulkar-collector-coin-limited-edition',
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for url in response.xpath("//ul[@class='pagination']/li[@class='pagination_next']/a/@href").getall():
            url = 'https://www.coinbazaar.in' + url
            yield scrapy.Request(url=url, callback=self.parse)

        for url in response.xpath("//div[@class='product-container']/div/div[@class='product-image-container']/a[@class='product_img_link']/@href").getall():
            print('Fetching item details: ' + url)
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        print('Inside parse for: ' + response.request.url)

        self.count += 1

        item = {}

        item['url'] = response.request.url
        item['title'] = response.xpath("//div[@class='pccwrapper']/h1/text()").get()
        item['sku'] = response.xpath("//div[@class='pccwrapper']/p[@id='product_reference']/span[@itemprop='sku']/text()").get()
        item['price'] = response.xpath("//p[@class='our_price_display']/span[@id='our_price_display']/@content").get()
        item['product'] = response.xpath("//input[@id='product_page_product_id']/@value").get()
        item['item'] = response.xpath("//input[@id='product_page_product_id']/@value").get()
        item[self.key_available] = True

        for i, row in enumerate(response.xpath("//div[@id='tabs-1']/div[@class='rte']/table/tbody/tr/td")):
            if i % 2 == 0:
                key = row.xpath(".//p/text()").get()
            else:
                if key is not None:
                    values = row.xpath(".//text()").getall()
                    value = values[0] if len(values) > 0 else ''
                    item[key] = value

        self.extract_table_rows(response.xpath("//div[@id='tabs-1']/div/table/tbody/tr/td"), item)
        self.extract_table_rows(response.xpath("//div[@id='tabs-2']/section/table/tr/td"), item)

        item['currency'] = 'INR'

        if 'MMTC-PAMP' in item['title']:
            if 'Gold' in item['title']:
                item_type = 'Gold'
            elif 'Silver' in item['title']:
                item_type = 'Silver'
            else:
                item_type = 'Other'

            if 'Ingot Bar' in item['title']:
                item_type = 'INGOT-' + item_type
            elif 'Casted Bar' in item['title']:
                item_type = 'INGOT-CASTED-' + item_type
            elif 'Bar' in item['title']:
                item_type = 'BAR-' + item_type
            elif 'Coin' in item['title']:
                item_type = 'COIN-' + item_type
            else:
                item_type = 'OTHER-' + item_type
        else:
            item_type = 'Other'

        if 'Purity' not in item:
            if 'Metal' in item:
                purity = self.extract_number(item['Metal'])
                if float(purity) < 100:
                    purity = 10 * float(purity)
                item['Purity'] = purity

        self.price_data.append({
            'type': item_type,
            'weight': item['Weight'],
            'purity': item['Purity'],
            'price': item['price']
        })

        self.price_items.append({
            'index': item['item'],
            'price': item['price']
        })

        image_urls = response.xpath("//div[@id='thumbs_list']/ul[@id='thumbs_list_frame']/li/a/@href").getall()
        item[self.key_images] = []

        for image_url in image_urls:
            item[self.key_images].append({
                'original_url': image_url,
                self.key_img_url: self.get_image_url(image_url)
            })

        self.items[self.get_0_index(item['item'])] = item
        
        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        return self.save_price_base_path() + '/coin-bazaar-data.json'

    def save_price(self):
        self.price_data.append({
            'data': self.price_items
        })

        self.save_price_hourly()
