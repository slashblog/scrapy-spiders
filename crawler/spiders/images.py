# (C) Copyright 2020 

#import cv2
import numpy as np
import os
import scipy

# References:
# https://github.com/moondra2017/Computer-Vision/blob/master/DHASH%20AND%20HAMMING%20DISTANCE_Lesson.ipynb
# https://www.youtube.com/watch?v=NzQDkQ6odK8
# https://www.youtube.com/watch?v=8rcVcH4OTdo
# https://www.pyimagesearch.com/2017/11/27/image-hashing-opencv-python/ (not used in this code) 

class ImageManager():
    key_images = 'images'
    key_img_url = 'url'
    key_image_score = 'score'
    key_duplicates = 'duplicates'
 
    #@classmethod
    # def img_gray(cls, image_path):
    #    image = cv2.imread(image_path)
    #    return np.average(image, weights=[0.299, 0.587, 0.114], axis=2)

    #@classmethod
    # def resize(cls, image, height=30, width=30):
    #    row_res = cv2.resize(image,(height, width), interpolation = cv2.INTER_AREA).flatten()
    #    col_res = cv2.resize(image,(height, width), interpolation = cv2.INTER_AREA).flatten('F')
    #    return row_res, col_res
    
    @classmethod
    def intensity_diff(cls, row_res, col_res):
        difference_row = np.diff(row_res)
        difference_col = np.diff(col_res)
        difference_row = difference_row > 0
        difference_col = difference_col > 0
        return np.vstack((difference_row, difference_col)).flatten()

    @classmethod
    def difference_score(cls, image_path, height = 30, width = 30):
        gray = cls.img_gray(image_path)
        row_res, col_res = cls.resize(gray, height, width)
        difference = cls.intensity_diff(row_res, col_res)
        
        return difference

    @classmethod
    def hamming_distance(cls, score1, score2):
        get_array = lambda score : [True if i =='1' else False for i in score]
        score1 = np.asarray(get_array(score1))
        score2 = np.asarray(get_array(score2))
        return scipy.spatial.distance.hamming(score1, score2)

    @classmethod
    def find_duplicates_and_uniques(cls, images):
        def update_duplicate(image1, image2):
            if cls.key_duplicates not in image1:
                image1[cls.key_duplicates] = []
            image1[cls.key_duplicates].append(image2[cls.key_img_url])

        for i, image1 in enumerate(images):
            for j, image2 in enumerate(images, i):
                distance = cls.hamming_distance(image1[cls.key_image_score], image2[cls.key_image_score])
                if (distance < 0.10):
                    update_duplicate(image1, image2)
                    update_duplicate(image2, image1)
