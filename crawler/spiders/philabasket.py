# (C) Copyright 2020

# scrapy crawl philabasket

import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class PhilaBasket(BaseSpider):
    name = "philabasket"

    def __init__(self, destination):
        super().__init__(destination, destination, True)
        self.old_items = self._get_existing_items()
        self.direct_links = ['https://www.philabasket.com/product/sl145-stepwell-stepwells-mix-sheetlet-mnh-india-2017-philabasket-com-buy-indian-stamps/']

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.philabasket.com/product-category/miniature_sheets/'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for url in response.xpath("//ul[@class='page-numbers']/li/a/@href").getall():
            yield scrapy.Request(url=url, callback=self.parse)

        for url in response.xpath("//div[@class='product-img-wrapper']/div/a/@href").getall():
            print('Fetching item details: ' + url)
            yield scrapy.Request(url=url, callback=self.parse_item)

        for url in self.direct_links:
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        print('Inside parse for: ' + response.request.url)

        index = response.xpath("//button[@name='add-to-cart']/@value").get()
        zero_index =  self.get_0_index(str(index))
        title = response.xpath("//div/h1/text()").get()
        price = response.xpath("//ins/span/bdi/text()").get()
        image_url = response.xpath("//div[@data-thumb]/a/@href").get().replace('–', '%E2%80%93')

        item = {}
        item['item'] = index
        item['title'] = title    
        item['price'] = price
        item['description'] = title
        item['original_url'] = image_url
        item['actual_url'] = response.request.url
        item['image_url'] = self.get_image_url(image_url)
        item[self.key_available] = True

        self.items[zero_index] = item
        self.titles.add(title)

        print('Image url: ' + image_url + ' => ' + self.get_image_url(image_url))

        image_urls = []
        image_urls.append(image_url)
        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        pass

    def save_price(self):
        pass
