# (C) Copyright 2020

# scrapy crawl sample

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class SampleSpider(BaseSpider):
    name = "sample"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

    def save_price_path(self):
        pass

    def save_price(self):
        pass
