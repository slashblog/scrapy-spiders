# (C) Copyright 2021 

# scrapy crawl mintageworld

import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class MintageWorldSpider(BaseSpider):
    name = "mintageworld"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.mintageworld.com/stamp/list/140/'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for url in response.xpath("//ul[@class='pagination']/li/a/@href").getall():
            url = response.urljoin(url)
            yield scrapy.Request(url=url, callback=self.parse)

        for url in response.xpath("//div[@class='product-details']/div[@class='view-more']/a/@href").getall():
            url = response.urljoin(url)
            print('Fetching item details: ' + url)
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        print('Inside parse for: ' + response.request.url)

        index = response.xpath("//input[@name='stamp_id']/@value").get()
        title = response.xpath("//div[@class='product-details']/div[@class='product-title']/text()").get()
        #price = response.xpath("//small[@aria-hidden='true']/text()").get()
        image_url = response.xpath("//ul/li/a/img[@class='etalage_source_image img-responsive']/@src").get()
        #print('Image url: ' + image_url)

        item = {}
        tds = response.xpath("//div[@class='product-details']/table/tr/td/text()").getall()
        for i in range(0, len(tds), 3):
            key = tds[i]
            value = tds[i + 2]
            item[key] = value

        item['item'] = index
        item['title'] = title    
        #item['price'] = price
        item['description'] = title
        item['original_url'] = image_url
        item['actual_url'] = response.request.url
        item['image_url'] = self.get_image_url(image_url)
        item[self.key_available] = True

        self.items[str(item['item'])] = item
        self.add_title(title)

        image_urls = []
        image_urls.append(image_url)
        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        pass

    def save_price(self):
        pass
