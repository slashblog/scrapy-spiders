# (C) Copyright 2020 

# scrapy crawl epostoffice

import re
import scrapy
import os

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class EpostOfficeSpider(BaseSpider):
    name = 'epostoffice'
    cookies = {}
    found_ids = []

    def __init__(self, destination):
        super().__init__(destination, destination, True)
        self.old_items = self._get_existing_items()

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.epostoffice.gov.in/Login.aspx?service=Philately'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        if 'previousIndex' not in response.meta:
            previous_index = -1
        else:
            previous_index = response.meta['previousIndex']
        yield scrapy.Request(url='https://www.epostoffice.gov.in/buy_Gueststamps.aspx?in=%d' % previous_index, dont_filter=True, callback=self.parse_list, meta={'previousIndex' : previous_index})

    def does_index_data_exist(self, index):
        zero_index = self.get_0_index(str(index))
        
        if zero_index in self.old_items:
            item = self.old_items[zero_index]
            item[self.key_available] = True
            self.items[zero_index] = item
            self._move_image(index, item)
            return True
        
        return False

    def _move_image(self, index, item):
        xpath = os.path.join(self.destination, 'data/india/images', str(index) + '.jpg')
        if os.path.exists(xpath):
            x_destination_path = os.path.join(self.images_store, item['image_url'])
            print('Image: ' + xpath + ' => ' + x_destination_path)
            dir = os.path.dirname(x_destination_path)
            if not os.path.isdir(dir):
                os.makedirs(dir)
            os.rename(xpath, x_destination_path)
            return True

        return False

    def _get_index_for_processing(self, index):
        if (index <= 0):
            return 0

        while self.does_index_data_exist(index):
            index = index - 1

        return index

    def parse_list(self, response):
        print('Inside parse for: ' + response.request.url)

        previous_index = response.meta['previousIndex']

        if previous_index > 0:
            index = self._get_index_for_processing(previous_index - 1)
        else:
            td = response.xpath("//table[@id='dgData']/tr/td/input[@type='image']/@src").get()
            splits = td.split('=')
            index = self._get_index_for_processing(int(splits[1]))

        if index > 0:
            yield scrapy.Request(url='https://www.epostoffice.gov.in/view_stamp.aspx?image=Handler.ashx?stamp_id=%d' % index, dont_filter=True, callback=self.parse_item, meta={'index' : index})
        else:
            print('All new items parsed. Now moving on to getting available items')
            yield scrapy.Request(url='https://www.epostoffice.gov.in/Login.aspx?service=Philately', dont_filter=True, callback=self.guest_login)

    def parse_item(self, response):
        print('Inside parse for: ' + response.request.url)

        index = response.meta['index']

        item = {}
        item['item'] = index
        item['title'] = response.xpath("//div/span[@id='lbldes1']/text()").get()    
        item['price'] = response.xpath("//div/span[@id='lbldes2']/text()").get()
        item['description'] = response.xpath("//div/span[@id='lbldes3']/text()").get()
        item['original_url'] = response.xpath("//div/img[@id='Image1']/@src").get()
        item['actual_url'] = response.urljoin(item['original_url'])
        item['image_url'] = self.get_image_url(item['actual_url'])
        item[self.key_available] = True

        #print(item)
        self.items[self.get_0_index(str(item['item']))] = item

        yield scrapy.Request(url='https://www.epostoffice.gov.in/Login.aspx?service=Philately?in=%d' % (index - 1), dont_filter=True, callback=self.parse, meta={'previousIndex' : index})

        if not self._move_image(index, item):
            image_urls = [item['actual_url']]
            yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def guest_login(self, response):
        yield scrapy.Request(url='https://www.epostoffice.gov.in/buy_Gueststamps.aspx', dont_filter=True, callback=self.parse_existence, meta={'page' : 1})

    def parse_existence(self, response):
        print('Inside parse for: ' + response.request.url)

        count = 0
        for url in response.xpath("//table[@id='dgData']/tr/td/input[@type='image']/@src").getall():
            id = url.split('=')[1]

            if id in self.found_ids:
                print('ID already exists: ' + id)
                continue

            self.found_ids.append(self.get_0_index(id))
            count = count + 1

        print('%d new ids found' % count)

        page = response.meta['page']
        page = page + 1
        found = False
        first = True
        next = None
        for a in response.xpath("//a[contains(@href, 'javascript:__doPostBack')]"):
            text = a.xpath("./text()").get()
            
            if text == 'Add to Order':
                continue
            
            if first and text == '...':
                first = False
                continue
            elif text == str(page):
                next = self.get_next_new(a)
                found = True
            elif not found and text == '...':
                next = self.get_next_new(a)
                found = True
            first = False

        if next is None:
            available_items = {}
            available_items[self.name] = self.found_ids
            self.save('available.json', available_items)
            return

        print('Page %d => %s' % (page, next))
        yield scrapy.FormRequest('https://www.epostoffice.gov.in/buy_Gueststamps.aspx', formdata=self.get_form_data(next, response), callback=self.parse_existence, meta={'page' : page})

    def get_next_new(self, a):
        href = a.xpath("./@href").get()
        match = re.search('dgData[$]ctl14[$]ctl[0-9][0-9]', href)
        next = href[match.start():match.end()]
        return next

    def get_form_data(self, val, response):
        view_state = response.xpath("//input[@id='__VIEWSTATE']/@value").get()
        view_state_generator = response.xpath("//input[@id='__VIEWSTATEGENERATOR']/@value").get()
        event_validation = response.xpath("//input[@id='__EVENTVALIDATION']/@value").get()

        return {
            'ctl00$ScriptManager1': 'ctl00$ContentPlaceHolder1$uppnlApplication_id|' + val,
            '__EVENTTARGET': val,
            '__EVENTARGUMENT': '',
            '__LASTFOCUS': '',
            '__VIEWSTATE': view_state,
            '__VIEWSTATEGENERATOR': view_state_generator,
            '__VIEWSTATEENCRYPTED': '',
            '__EVENTVALIDATION': event_validation,
            'ctl00$hidden1': '',
            'ctl00$ddlLanguage': 'en-US',
            'ctl00$ContentPlaceHolder1$HiddenField1': '',
            'ctl00$ContentPlaceHolder1$hdnDetail': '',
            '__ASYNCPOST': 'false'
        }        

    def save_price_path(self):
        pass

    def save_price(self):
        pass
