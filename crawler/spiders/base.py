# (C) Copyright 2020 

import ast
import hashlib
import json
import os
import re
import scrapy

from abc import ABC, abstractmethod
from crawler.spiders.images import ImageManager
from datetime import datetime
from os import path
from scrapy import signals
from scrapy.utils.project import get_project_settings
from scrapy.utils.python import to_bytes

class BaseSpider(scrapy.Spider, ABC, ImageManager):
    items = {}
    titles = set()
    price_data = []
    count = 0
    key_available = 'available'
    key_images = 'images'
    key_creation_timestamp = 'creation_timestamp'
    key_modification_timestamp = 'modification_timestamp'
    key_index = 'index'
    key_datetime = 'datetime'
    key_latest_price = 'latest'
    key_old_price = 'old'
    key_price = 'price'
    date_stamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    def __init__(self, destination, price_destination, only_item_processing = False):
        self.destination = destination
        self.price_destination = price_destination
        self.only_item_processing = only_item_processing

        if not path.isdir(destination):
            print('Destination directory: ' + destination + ' does not exist.')
            raise scrapy.exceptions.CloseSpider('destination not found')

        if not path.isdir(destination):
            print('Destination directory: ' + destination + ' does not exist.')
            raise scrapy.exceptions.CloseSpider('destination not found')

        self.images_store = os.path.join(destination, 'images')
        if not path.exists(self.images_store):
            os.makedirs(self.images_store)
        print('Setting images store path to: ' + self.images_store)
        settings = get_project_settings()
        settings.set('IMAGES_STORE', self.images_store)

        self.titles_path = os.path.join(self.destination, 'titles.txt')
        if path.exists(self.titles_path):
            with open(self.titles_path, 'r') as file:
                self.titles = ast.literal_eval(file.read())

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(BaseSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
        return spider

    @classmethod
    def extract_number(cls, value):
        return re.sub(r'[^.0-9]', '', value.strip())

    @classmethod
    def extract_text(cls, value):
        return re.sub(r'[^-_.,:+ 0-9a-zA-Z]', '', value.strip())

    @classmethod
    def extract_table_rows(cls, cells, item):
        for i, row in enumerate(cells):
            if i % 2 == 0:
                key = row.xpath(".//descendant-or-self::text()").get()
            else:
                if key is not None:
                    key = cls.extract_text(re.sub(r'[ :]+$', '', key.strip()))
                    value = row.xpath(".//descendant-or-self::text()").get()
                    if value is not None:
                        item[key] = cls.extract_text(value)

    @classmethod
    def print_variable(cls, variable):
        print(json.dumps(variable, indent=4, sort_keys=True))

    def load(self, file_path):
        if path.exists(file_path):
            with open(file_path, 'r') as file:
                return json.load(file)
            return {}
        else:
            return {}

    def save(self, file_relative_path, items):
        file_path = os.path.join(self.destination, file_relative_path)
        print('Saving ' + str(len(items)) + ' items to: ' + file_path)
        json_string = json.dumps(items, indent=4, sort_keys=True)
        #print(json_string)
        with open(file_path, "wb") as file:
            file.write(to_bytes(json_string))

    def add_title(self, title):
        print('Title add: ' + title)
        self.titles.add(title)

    def add_item(self, group, index):
        if self.get_name() not in group:
            group[self.get_name()] = []
        if index not in group[self.get_name()]:
            group[self.get_name()].append(index)

    @classmethod
    def hexdigest(cls, text):
        return hashlib.sha1(to_bytes(text)).hexdigest()

    @classmethod
    def equal(cls, one, two):
        one_string = cls.hexdigest(json.dumps(one, sort_keys=True))
        two_string = cls.hexdigest(json.dumps(two, sort_keys=True))
        return one_string == two_string

    def _get_existing_items(self):
        file_path = self.get_file_name()
        return self.load(path.join(self.destination, file_path))

    def update_for_old_items(self):
        file_path = self.get_file_name()
        incoming = self.load(path.join(self.destination, 'incoming.json'))
        outgoing = self.load(path.join(self.destination, 'outgoing.json'))
        available = self.load(path.join(self.destination, 'available.json'))
        unavailable = self.load(path.join(self.destination, 'unavailable.json'))

        print('Retrieved ' + str(len(self.items)) + ' items')
        old_items = self._get_existing_items()
        print('Loaded ' + str(len(old_items)) + ' saved from earlier save')
        #print('Old items:')
        #print(json.dumps(old_items, indent=4, sort_keys=True))
        #print('New items:')
        #print(json.dumps(self.items, indent=4, sort_keys=True))
        print('Processing existing items')
        for index in old_items:
            if index in self.items:
                #print(index + ' exists in new items')
                if self.key_available not in old_items[index]:
                    if self.key_available in self.items[index]:
                        if self.items[index][self.key_available] == True:
                            #print('Adding ' + index + ' to available')
                            self.add_item(available, index)
                        else:
                            #print('Adding ' + index + ' to unavailable')
                            self.add_item(unavailable, index)
                else:
                    if self.key_available in self.items[index]:
                        if self.items[index][self.key_available] == True and old_items[index][self.key_available] == False:
                            #print('Adding ' + index + ' to unavailable')
                            self.add_item(unavailable, index)
                        if self.items[index][self.key_available] == False and old_items[index][self.key_available] == True:
                            #print('Adding ' + index + ' to available')
                            self.add_item(available, index)

                if self.key_creation_timestamp in old_items[index]:
                    self.items[index][self.key_creation_timestamp] = old_items[index][self.key_creation_timestamp]
                else:
                    self.items[index][self.key_creation_timestamp] = self.date_stamp

                self.items[index][self.key_modification_timestamp] = old_items[index][self.key_modification_timestamp]
                if self.equal(self.items[index], old_items[index]):
                    self.items[index][self.key_modification_timestamp] = old_items[index][self.key_modification_timestamp]
                else:
                    #print('Item modified: ' + index)
                    self.items[index][self.key_modification_timestamp] = self.date_stamp
            else:
                #print(index + ' not found in existing items')

                if self.key_image_score in old_items[index]:
                    del old_items[index][self.key_image_score]

                self.add_item(outgoing, index)
                self.items[index] = old_items[index]
                self.items[index][self.key_available] = False
                if self.key_creation_timestamp not in old_items[index]:
                    self.items[index][self.key_creation_timestamp] = self.date_stamp
                if self.key_modification_timestamp not in old_items[index]:
                    self.items[index][self.key_modification_timestamp] = self.date_stamp

        print('Processing new items')
        for index in self.items:
            if index not in old_items:
                print('Found new item: ' + index)
                print('Adding ' + index + ' to incoming')
                self.add_item(incoming, index)
                self.items[index][self.key_creation_timestamp] = self.date_stamp
                self.items[index][self.key_modification_timestamp] = self.date_stamp

                if self.key_available in self.items[index] and self.items[index][self.key_available] == True:
                    print('Adding ' + index + ' to available')
                    self.add_item(available, index)

        #self.update_image_score()

        if not self.only_item_processing:
            self.save('incoming.json', incoming)
            self.save('outgoing.json', outgoing)
            self.save('available.json', available)
            self.save('unavailable.json', unavailable)
        self.save(file_path, self.items)

    def update_image_score(self):
        def get_score(image_url):
            image_path = os.path.join('images', image_url)
            print('Processing score for ' + image_path)

            if os.path.exists(image_path):
                return ''.join(['1' if x else '0' for x in self.difference_score(image_path).tolist()])
            else:
                print('File not found: ' + image_path)
                return ''

        for index in self.items:
            item = self.items[index]
            if self.key_images in item:
                images = []
                for image in item[self.key_images]:
                    if type(image) is dict:
                        if self.key_image_score not in image:
                            image[self.key_image_score] = get_score(image[self.key_img_url])
                        images.append(image)
                    else:
                        score = get_score(image)
                        images.append({
                            self.key_img_url: image,
                            self.key_image_score: score
                        })
                item[self.key_images] = images

    def spider_closed(self, spider):
        if not self.items:
            print('No data was collected. Skipping saving of items.')
            return
            
        #self.update_image_score()
        self.update_for_old_items()
        self.save_price()

        print('titles: ' + str(len(self.titles)))
        with open(self.titles_path, 'w') as file:
            file.write(str(self.titles))
            #for title in self.titles:
            #    print('Writing: ' + title)
            #    file.write(title + '\n')

    @classmethod
    def get_image_url(cls, image_url):
        return 'full/%s.jpg' % cls.hexdigest(image_url)

    @abstractmethod
    def get_name(self):
        pass

    def get_file_name(self):
        return self.get_name() + '.json'

    @abstractmethod
    def get_urls(self):
        pass

    @classmethod
    def get_0_index(cls, index):
        return index.zfill(6)

    def start_requests(self):
        urls = self.get_urls()
        
        for url in urls:
            print("Processing " + url)
            yield scrapy.Request(url=url, callback=self.parse)

    @abstractmethod
    def parse(self, response):
        pass

    def save_price_base_path(self):
        now = datetime.now()
        save_price_path = path.join(self.price_destination, 'data', now.strftime('%Y/%m'))
        print('save_price_path: ' + save_price_path)
        if not path.isdir(save_price_path):
            os.makedirs(save_price_path)
        return save_price_path

    @abstractmethod
    def save_price_path(self):
        pass

    @abstractmethod
    def save_price(self):
        pass

    def save_price_data(self, type):
        if len(self.price_data) > 0:
            data = self.load(self.save_price_path())
            now = datetime.now()

            if type == 'daily':
                index = now.strftime('%Y-%m-%d')
                data[index] = self.price_data
            elif type == 'hourly-hyphenated':
                index = now.strftime('%Y-%m-%d-%H')
                data[index] = self.price_data
            elif type == 'hourly':
                index = now.strftime('%Y-%m-%d')
                if index not in data:
                    data[index] = {}
                data[index][now.strftime('%H')] = self.price_data
            
            self.save(self.save_price_path(), data)
        else:
            print('No price data to save. Skipping')

    def save_price_daily(self):
        self.save_price_data('daily')

    def save_price_hourly_hyphenated(self):
        self.save_price_data('hourly-hyphenated')

    def save_price_hourly(self):
        self.save_price_data('hourly')

    def save_price_on_change(self):
        if len(self.price_data) > 0:
            data = self.load(self.save_price_path())
            now = datetime.now()

            for price_item in self.price_data:
                index = price_item[self.key_index]
                price_changed = False
                if index in data:
                    if self.key_latest_price in data[index]:
                        for price_index in price_item:
                            if price_index in data[index][self.key_latest_price]:
                                if data[index][self.key_latest_price][price_index] != price_item[price_index]:
                                    price_changed = True
                                    break
                            else:
                                price_changed = True
                                break

                        if not price_changed:
                            for price_index in data[index][self.key_latest_price]:
                                if price_index == self.key_datetime:
                                    continue

                                if price_index not in price_item:
                                    price_changed = True
                                    break

                        if price_changed:
                            if self.key_old_price not in data[index]:
                                data[index][self.key_old_price] = []
                            data[index][self.key_old_price].append(data[index][self.key_latest_price])
                    else:
                        price_changed = True
                else:
                    price_changed = True

                if price_item:
                    price_item[self.key_datetime] = now.strftime('%Y-%m-%d %H:%M:%S')
                    if index not in data:
                        data[index] = {}
                    data[index][self.key_latest_price] = price_item

            self.save(self.save_price_path(), data)
        else:
            print('No price data to save. Skipping')
