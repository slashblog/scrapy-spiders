# (C) Copyright 2021 

# scrapy crawl collectorbazar

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class CollectorbazarSpider(BaseSpider):
    name = "collectorbazar"

    def __init__(self, destination):
        super().__init__(destination, destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.collectorbazar.com/item/india-1974-centenary-of-universal-postal-union-set-of-3-mnh-277375/',
            'https://www.collectorbazar.com/item/109-set-of-6-miniature-sheets-indian-cinema-100-years-cinema-film-music-2013-mnh-7414/'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)
        
        index = response.xpath("//input[@id='articleno']/@value").get()
        title = response.xpath("//div[@class='col-md-6 product-single-info']/h2/text()").get()
        price = ''.join(response.xpath("//div[@class='price']/text()").getall()).strip()
        image_urls = response.xpath("//div[@id='gal1']/a/@src").getall()
        #print('Image url: ' + image_url)

        item = {}
        item['item'] = index
        item['title'] = title    
        item['price'] = price
        item['description'] = title
        item['original_url'] = image_urls
        item['actual_url'] = response.request.url
        item['image_url'] = []
        for image_url in image_urls:
            item['image_url'].append(self.get_image_url(image_url))
        item[self.key_available] = True

        self.items[str(item['item'])] = item
        self.add_title(title)

        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        pass

    def save_price(self):
        pass
