# (C) Copyright 2020 

# scrapy crawl mmtcpamp

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class MmtcPampSpider(BaseSpider):
    name = "mmtcpamp"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.mmtcpamp.com/products-services/minted'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for itemDiv in response.xpath("//div[@class='modal  fade']"):
            index = itemDiv.xpath("./@id").get()

            for div in itemDiv.xpath("./div[@class='modal-dialog']/div[@class='modal-content']/div[@class='modal-body']"):
                title = div.xpath("./div[@class='row']/div/h6/text()").get().strip()
                image_urls = div.xpath("./div[@class='row']/div/div/img/@src").getall()
                image_names = div.xpath("./div[@class='row']/div/div/span/text()").getall()
                description = div.xpath("./div[@class='row']/div/div[@class='tab-content']/div/p[@class='productdesc-ttl']/text()").get().strip()
                attributeTexts = div.xpath("./div[@class='row']/div/div[@class='tab-content']/div/div[@class='row']/div/ul/li/span/text()").getall()

                item = {}
                item['index'] = index
                item['title'] = title
                item['description'] = description
                item[self.key_available] = True

                for attributeText in attributeTexts:
                    splits = attributeText.split(' ')
                    if (len(splits) >= 2):
                        item[splits[0]] = ''
                        for i in range(1, len(splits)):
                            item[splits[0]] += splits[i]
                    else:
                        print('Skipping attribute ' + attributeText)

                item[self.key_images] = []
                for i, image_url in enumerate(image_urls):
                    image = {}
                    image['original_url'] = image_url
                    image['name'] = image_names[i]
                    image['url'] = self.get_image_url(image_url)
                    item[self.key_images].append(image)
                
                yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

                self.items[self.get_0_index(index)] = item

    def save_price_path(self):
        pass

    def save_price(self):
        pass
