# (C) Copyright 2020 

# scrapy crawl sample

import os
import scrapy
import uuid

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem
from os import path

class PostageStampsSpider(BaseSpider):
    name = "postage_stamps"

    def __init__(self, destination):
        super().__init__(destination, destination, True)
        self.pdf_destination = os.path.join(destination, 'pdfs')
        if not path.exists(self.pdf_destination):
            print('Making directory: ' + self.pdf_destination)
            os.makedirs(self.pdf_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        urls = [
            'http://postagestamps.gov.in/Stamps2001.aspx',
            'http://postagestamps.gov.in/Stamps2002.aspx',
            'http://postagestamps.gov.in/Stamps2003.aspx',
            'http://postagestamps.gov.in/Stamps2004.aspx',
            'http://postagestamps.gov.in/Stamps2005.aspx',
            'http://postagestamps.gov.in/Stamps2006.aspx',
            'http://postagestamps.gov.in/Stamps2007.aspx',
            'http://postagestamps.gov.in/Stamps2008.aspx',
            'http://postagestamps.gov.in/Stamps2009.aspx',
            'http://postagestamps.gov.in/Stamps2010.aspx',
            'http://postagestamps.gov.in/Stamps2011.aspx',
            'http://postagestamps.gov.in/Stamps2012.aspx',
            'http://postagestamps.gov.in/Stampsofyear.aspx?uid=2013',
            'http://postagestamps.gov.in/Stampsofyear.aspx?uid=2014',
            'http://postagestamps.gov.in/Stampsofyear.aspx?uid=2015',
            'http://postagestamps.gov.in/Stampsofyear.aspx?uid=2016',
            'http://postagestamps.gov.in/Stampsofyear.aspx?uid=2017',
            'http://postagestamps.gov.in/Stampsofyear.aspx?uid=2018',
            'http://postagestamps.gov.in/Stampsofyear.aspx?uid=2019',
            'http://postagestamps.gov.in/Stampsofyear.aspx?uid=2020'
        ]

        return urls

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        if len(response.xpath("//span[@id='ctl00_RightImages_lblYear']/text()")):
            year = response.xpath("//span[@id='ctl00_RightImages_lblYear']/text()").get()
        else:
            year = response.xpath("//h1/text()").get().split()[1]
        image_urls = []

        for tr in response.xpath("//div[@id='rows_container']/div/center/table/tr/td/table/tr/td/table/tr"):
            item = {}
            item['year'] = year
            item['original_url'] = tr.xpath("./td/img/@src").get()
            item['actual_url'] = response.urljoin(item['original_url'])
            item['image_url'] = self.get_image_url(item['actual_url'])
            item['release_date'] = ' '.join(response.xpath("//div[@id='rows_container']/div/center/table/tr/td/table/tr/td/table/tr")[0].xpath("./td/p/span[contains(@id, 'date')]/text()").get().split())
            item['title'] = tr.xpath("./td/p/span[contains(@id, 'Label')]/text()").get()
            item['price'] = tr.xpath("./td/div/table/tr/td/span[contains(@id, 'price')]/text()").get()
            item[self.key_available] = True

            id = ''
            if len(tr.xpath("./td/div/table/tr/td/a[contains(@id, 'HyperLink')]/@href")) > 0:
                item['pdf_src_url'] = tr.xpath("./td/div/table/tr/td/a[contains(@id, 'HyperLink')]/@href").get()
                item['pdf_url'] = response.urljoin(item['pdf_src_url'])
                splits = item['pdf_src_url'].split('=')
                if len(splits) > 1:
                    id = splits[1]
                    index = year + '-' + self.get_0_index(id)
                pdf_file = path.join(self.pdf_destination, id + '.pdf')
                if not path.exists(pdf_file) or os.stat(pdf_file).st_size == 0:
                    command = 'wget -c -O "' + pdf_file + '" "' + item['pdf_url'] + '"'
                    print('Executing command: ' + command)
                    os.system(command)

            if id == '':
                index = id = year + '-' + self.hexdigest(item['original_url'])

            item['index'] = id

            image_urls.append(item['actual_url'])
            self.items[index] = item

        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def parse_stamps(self, response):
        print('Inside parse for: ' + response.request.url)

        if len(response.xpath("//span[@id='ctl00_RightImages_lblYear']/text()")):
            year = response.xpath("//span[@id='ctl00_RightImages_lblYear']/text()").get()
        else:
            year = response.xpath("//h1/text()").get().split()[1]
        image_urls = []

        i = 1
        for tr in response.xpath("//div[@class='cntr']/table/tr"):
            item = {}
            item['year'] = year
            item['original_url'] = tr.xpath("./td/a/img/@src").get()
            item['actual_url'] = response.urljoin(item['original_url'])
            item['image_url'] = self.get_image_url(item['actual_url'])
            print('image_url: ' + str(i) + item['original_url'])
            i = i + 1

            ps = tr.xpath("./td/p/text()").getall()
            if len(ps) > 0:
                item['release_date'] = ps[0].strip().split(':')[0]
            if len(ps) > 1:
                item['title'] = ps[1].strip()
                for i in range(2, len(ps)):
                    item['title'] = item['title'] + ' ' + ps[i].strip()

            item['price'] = tr.xpath("./td/div/table/tr/td/text()").getall()[1].split()[1].strip()
            item[self.key_available] = True

            index = id = year + '-' + self.hexdigest(item['original_url'])

            item['index'] = id

            image_urls.append(item['actual_url'])
            self.items[index] = item
 
        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        pass

    def save_price(self):
        pass
