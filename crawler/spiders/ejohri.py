# (C) Copyright 2020 

# scrapy crawl ejohri

import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class EjohriSpider(BaseSpider):
    name = "ejohri"

    def __init__(self, destination, price_destination):
        super().__init__(destination, price_destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.ejohri.com/4s-gold-enterprises/shop-by/products-type-gold-coins-silver-coins-coins-and-bars',
            'https://www.ejohri.com/prakash-jewellers/shop-by/products-type-coins-and-bars',
            'https://www.ejohri.com/regalia-jewellery-house',
            'https://www.ejohri.com/silvera'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for url in response.xpath("//ul[@class='items pages-items']/li[@class='item pages-item-next']/a/@href").getall():
            index = url.split('?p=')[1]
            next_url = response.request.url + '?p=' + str(index)
            print('Next url: ' + next_url)
            yield scrapy.Request(url=next_url, callback=self.parse)            

        for url in response.xpath("//ol[@class='products list items product-items product-items-page']/li[@class='item product product-item']/div/a[@class='product photo product-item-photo image_on_normal']/@href").getall():
            print('Fetching item details: ' + url)
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        print('Inside parse item for: ' + response.request.url)

        self.count += 1

        item = {}

        temp = "//div[@class='product-details-brand']/div[@class='product-details-brand-wrapper']/div[@class='product-details-info']"
        item['url'] = response.request.url
        item['vendor_name'] = response.xpath(temp + "/div[@class='product-details-name']/text()").get().strip()
        item['vendor_desc'] = response.xpath(temp + "/div[@class='product-detials-dec']/text()").get().strip()
        item['title'] = response.xpath("//div[contains(@class, 'page-title-wrapper product')]/h1[@class='page-title']/span/text()").get().strip()
        item['product'] = response.xpath("//div[@class='product-add-form']/form/input[@name='product']/@value").get().strip()
        item['item'] = response.xpath("//div[@class='product-add-form']/form/input[@name='item']/@value").get().strip()
        item['sku'] = response.xpath("//div[@class='product attribute sku']/div[@class='value']/text()").get().strip()
        item['description'] = response.xpath("//div[@class='product attribute overview']/div[@class='value']/text()").get().strip()
        item['price'] = response.xpath("//span[@class='price-container price-final_price tax weee']/meta[@itemprop='price']/@content").get().strip()
        item['currency'] = response.xpath("//span[@class='price-container price-final_price tax weee']/meta[@itemprop='priceCurrency']/@content").get().strip()
        item[self.key_available] = True

        self.price_data.append({
            'name': item['title'],
            'id': item['item'],
            'price': item['price'],
            'category': item['vendor_name']
        })

        for row in response.xpath("//table[@id='product-attribute-specs-table']/tbody/tr"):
            key = row.xpath("./th[@class='col label']/text()").get().strip()
            value = row.xpath("./td[@class='col data']/text()").get().strip()
            item[key] = value

        image_urls = response.xpath("//div[@class='product_thumbnail']/div/div[@class='item ']/img/@src").getall()
        item[self.key_images] = []
        for image_url in image_urls:
            item[self.key_images].append({
                'original_url': image_url,
                self.key_img_url: self.get_image_url(image_url)
            })

        vendor_image_url = response.xpath("//div[@class='product-details-brand']/div[@class='product-details-brand-wrapper']/div[@class='product-details-brand-image']/img/@src").get()
        image_urls.append(vendor_image_url)
        item['vendor_image'] = self.get_image_url(vendor_image_url)

        self.items[self.get_0_index(item['item'])] = item
        
        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        return self.save_price_base_path() + '/ejohri-data.json'

    def save_price(self):
        self.save_price_daily()
