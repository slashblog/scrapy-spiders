# (C) Copyright 2020 

# scrapy crawl sample

import scrapy

from crawler.spiders.base import BaseSpider
from crawler.items import ImageItem

class BharatPhilatelySpider(BaseSpider):
    name = "bharatphilately"

    def __init__(self, destination):
        super().__init__(destination, destination)

    def get_name(self):
        return self.name
    
    def get_urls(self):
        return [
            'https://www.bharatphilately.com/format/miniature-sheets/'
        ]

    def parse(self, response):
        print('Inside parse for: ' + response.request.url)

        for url in response.xpath("//a[@class='page-numbers']/@href").getall():
            url = response.urljoin(url)
            yield scrapy.Request(url=url, callback=self.parse)

        for url in response.xpath("//h3[@class='product-name']/a/@href").getall():
            url = response.urljoin(url)
            print('Fetching item details: ' + url)
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        print('Inside parse for: ' + response.request.url)

        index = response.xpath("//input[@name='product_id']/@value").get()
        title = response.xpath("//head/title/text()").get()
        price = response.xpath("//p[@class='price']/ins/span[@class='woocommerce-Price-amount amount']/text()").get()
        image_url = response.xpath("//div[@class='woocommerce-product-gallery__image']/a/@href").get()
        #print('Image url: ' + image_url)

        item = {}
        item['item'] = index
        item['title'] = title    
        item['price'] = price
        item['description'] = title
        item['original_url'] = image_url
        item['actual_url'] = response.request.url
        item['image_url'] = self.get_image_url(image_url)
        item[self.key_available] = True

        self.items[str(item['item'])] = item
        self.add_title(title)

        image_urls = []
        image_urls.append(image_url)
        yield ImageItem(image_urls=image_urls, image_save_path=self.images_store)

    def save_price_path(self):
        pass

    def save_price(self):
        pass
