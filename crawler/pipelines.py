# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import os

from scrapy.pipelines.images import ImagesPipeline

class DownloadImagesPipeline(ImagesPipeline):
    image_save_path = ''

    def get_media_requests(self, item, info):
        self.image_save_path = item['image_save_path']
        print('Will save images into: ' + self.image_save_path)
        return super().get_media_requests(item, info)

    def file_path(self, request, response=None, info=None):
        save_path = os.path.join(self.image_save_path, super().file_path(request, response=response, info=info))
        print(request.url + ' => ' + save_path)
        return save_path

class MmtcpampPipeline(object):
    def process_item(self, item, spider):
        return item
