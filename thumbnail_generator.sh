#!/bin/bash

# (C) Copyright 2020 


DIR=$1

IMAGE_DIR="$DIR/images"
IMAGE_THUMB_DIR="$IMAGE_DIR/thumb"
IMAGE_INDEX_DIR="$IMAGE_DIR/index"
INDEX_SIZE=250x359

mkdir -p "$IMAGE_THUMB_DIR"
mkdir -p "$IMAGE_INDEX_DIR"

for IMAGE_IN in "$IMAGE_DIR/full"/*
do
    IMAGE_NAME=$(basename "$IMAGE_IN")
    IMAGE_OUT="$IMAGE_THUMB_DIR/$IMAGE_NAME"
    if [ ! -f "$IMAGE_OUT" ]
    then
        echo "$IMAGE_IN => $IMAGE_OUT"
        convert "$IMAGE_IN" -resize 80x80 "$IMAGE_OUT"
    fi

    IMAGE_OUT="$IMAGE_INDEX_DIR/$IMAGE_NAME"
    if [ ! -f "$IMAGE_OUT" ]
    then
        echo "$IMAGE_IN => $IMAGE_OUT"
        convert "$IMAGE_IN" -resize $INDEX_SIZE -gravity center -background 'rgb(255,255,255)' -extent $INDEX_SIZE "$IMAGE_OUT"
    fi
done
