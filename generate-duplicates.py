import json
import os
import sys

from imagededup.methods import CNN
from scrapy.utils.python import to_bytes

image_dir = os.path.join(sys.argv[1], 'images/full')
file_path = os.path.join(sys.argv[1], 'duplicate-images.json')

print('Json path: ' + file_path)
print('Images directory: ' + image_dir)

modified = False
if os.path.exists(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)

        for file_name in os.listdir(image_dir):
            if file_name not in data:
                print('New files discovered. Will regenerate duplication data')
                modified = True
                break

if not modified:
    print('No new images in: ' + image_dir)
    sys.exit(0)

cnn = CNN()

print(image_dir)
encodings = cnn.encode_images(image_dir=image_dir)
duplicates = cnn.find_duplicates(encoding_map=encodings)

print('Saving ' + file_path)
json_string = json.dumps(duplicates, indent=4, sort_keys=True)
with open(file_path, 'wb') as file:
    file.write(to_bytes(json_string))
